<?php

function msg($log_text)
{
    //Use output buffering to ensure the messages are shown while execution continues
    ob_start();
    echo '[' . date("m/d/Y H:i:s") . ']: ' . $log_text . "<br/>\n";
    ob_end_flush();
    ob_flush();
    flush();
}

function get_ordinal($number)
{
    //Get the appropriate ordinal value & suffix based on the number
    return date('jS', mktime(1, 1, 1, 1, $number));
}

function get_plural($number, $string)
{
    //Apply the appropriate plural suffix to string based on the number
    if ($number == 1) {
        return $string;
    } else {
        return $string . 's';
    }
}

function get_years_ago($years_ago)
{
    //Return the appropriate description of number of years ago
    if ($years_ago == 0)
    {
        return 'Earlier This Year';
    } else {
        return $years_ago . ' ' . get_plural($years_ago, "Year") . " Ago";
    }
}

function debug($variable, $exit = true) //only used for debugging purposes
{
    //Output the variable for debugging
    echo "<pre>\n";
    var_export($variable);
    echo "</pre>\n";
    if ($exit) {
        exit(0);
    }
}

?>