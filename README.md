# Flickr Photos On This Day

## Description
This is a web app written in PHP that uses the [Flickr API](https://www.flickr.com/services/api/) to show the pictures taken on the current day of the year in either HTML or RSS format.  It also generates an index page with links to generated pages for each day of the year.

## Installation & Setup
Follow these steps to setup the application:
1. Clone this repository into the desired folder on your web server.
1. Copy `config_TEMPLATE.php` to `config.php`.  Set the configuration values in `config.php` to the values corresponding to your Flickr account, API key, and your site.
1. Copy `theme_header_TEMPLATE.php` to `theme_header.php`.  Replace the contents of `theme_header.php` with your site's theme header.
1. Copy `theme_footer_TEMPLATE.php` to `theme_footer.php`.  Replace the contents of `theme_footer.php` with your site's theme footer.
1. Schedule a daily cron job that runs `generate_all_files.php`.  This will generate the RSS and HTML files.
1. To generate the album & collection data cache, run script `generate_album_cache.php`.  This script will create files `cached_album_list.json`, `cached_album_items.json`, & `cached_collection_list.json` holding the album & collection data so that we can avoid the excessive API calls during the daily cron job.  This only needs to be run any time new albums or collections are created on Flickr.

## Usage
The daily cron job will generate the RSS file and all of the HTML files.  The [Flickr Developer Guide](https://www.flickr.com/services/developer/api) states that API accesses should be kept under 3600 queries per hour.  To prevent the unlikely possibility that high traffic would exceed this limit, this app was designed to only access the API once per day and generate static HTML/RSS files that reference the fetched photo data.

The following files are generated:
- An HTML page for each day of the year is generated with the nomenclature `M-D.html`, where M is month and D is day of the month.
- The current day's page is copied into `today.html`. 
- The RSS file containing data from the current day is saved using the filename specified in `config.php`.
- An index page containing links to every day of the year is generated into `index.html`.

## Support
No support is offered.

## Authors and acknowledgment
Copyright (c) 2024 R. Andrew Love (unless explicitly stated otherwise).

## License
This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see http://www.gnu.org/licenses/.