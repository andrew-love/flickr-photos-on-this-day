<?php

//Global configuration values
const FLICKR_API_KEY = '';
const RSS_FILE_NAME = '';
const SITE_URL = '';
const FLICKR_USER_NAME = '';    //Flickr user who's photos will be retrieved
const START_YEAR = '';          //First year for which it will start fetching photos
define("END_YEAR", date("Y"));  //Last year for which it will fetch photos

?>