<?php

class RssWriter
{
    private $filename, $rssfile;

    public function __construct($file_in = null)
    {
        //Remember the file name for writing later
        $this->filename = $file_in;
    }

    public function open_file($title = null, $link = null, $description = null)
    {
        //Create RSS file
        $this->rssfile = fopen($this->filename, "w") or die("Unable to open rss file!");
        fwrite($this->rssfile, "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n");
        fwrite($this->rssfile, "<rss version=\"2.0\">\n");
        fwrite($this->rssfile, "<channel>\n");
        fwrite($this->rssfile, "\t<title>" . $title . "</title>\n");
        fwrite($this->rssfile, "\t<link><![CDATA[ " . $link . " ]]></link>\n");
        fwrite($this->rssfile, "\t<description>" . $description . "</description>\n");
    }

    public function add_item($title = null, $link = null, $description = null)
    {
        //Write the RSS item data
        fwrite($this->rssfile, "\t<item>\n");
        fwrite($this->rssfile, "\t\t<title><![CDATA[ " . $title . " ]]></title>\n");
        fwrite($this->rssfile, "\t\t<link>" . $link . "</link>\n");
        fwrite($this->rssfile, "\t\t<pubDate>" . date("D, d M Y H:i:s T") . "</pubDate>\n");
        fwrite($this->rssfile, "\t\t<description><![CDATA[ " . $description . " ]]></description>\n");
        fwrite($this->rssfile, "\t</item>\n");
    }

    public function close_file()
    {
        //Close the RSS file
        fwrite($this->rssfile, "</channel>\n");
        fwrite($this->rssfile, "</rss>\n");
        fclose($this->rssfile);
    }

    public static function summary_title($month, $day, $count, $numberOfYears)
    {
        $month_name = date("F", mktime(0, 0, 0, $month, 1));
        $day_ord = get_ordinal($day);

        //Return the header for the day of the year
        if ($count > 0) {
            $photos = get_plural($count, "Photo");
            $years = get_plural($numberOfYears, "Year");
            return "Flickr Photos On $month_name $day_ord - $count $photos from $numberOfYears $years";
        } else {
            return "Flickr Photos On $month_name $day_ord";
        }
    }

    public static function summary_body($summaryTitle, $albumSummary)
    {
        //Return the summary title followed by the album summary
        return "<h1><u>$summaryTitle</u></h1>\n$albumSummary";
    }
}

?>