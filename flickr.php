<?php

class Flickr {

    private $apiKey;
    private $album_list;
    private $album_items;
    private $collection_list;

    public function __construct($apikey = null) {
        $this->apiKey = $apikey;
        $this->album_list = [];
        $this->album_items = [];
        $this->collection_list = [];
    }

    public function search($query = null, $user_id = null, $per_page = 50, $taken_date = null, $year = null, $page = 1, $format = 'php_serial') {
        $args = array(
            'method' => 'flickr.photos.search',
            'api_key' => $this->apiKey,
            'text' => urlencode($query),
            'user_id' => $user_id,
            'per_page' => $per_page,    //max allowed per page is 500
            'page' => $page,  //default to 1 when omitted
            'format' => $format,
            'extras' => 'description, date_taken, views, geo, media, o_dims'
        );
        if ($taken_date != null && $year == null) {
            $args['min_taken_date'] = $taken_date . ' 00:00:00';
            $args['max_taken_date'] = $taken_date . ' 23:59:59';
        } else if ($taken_date == null && $year != null) {
            $args['min_taken_date'] = $year . '-01-01 00:00:00';
            $args['max_taken_date'] = $year . '-12-31 23:59:59';
        }

        $url = 'http://flickr.com/services/rest/?';
        $search = $url.http_build_query($args);
        $result = $this->file_get_contents_curl($search);
        if ($format == 'php_serial') $result = unserialize($result);
        return $result;
    }

    private function file_get_contents_curl($url) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_AUTOREFERER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.63 Safari/537.36');
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);	// ignore SSL errors
        $data = curl_exec($ch);
        $retcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        //sleep(1); //sleep 1 second between calls to avoid excessive request rates; disabled for now
        if ($retcode == 200) {
            return $data;
        } else {
            return null;
        }
    }

    public function get_video_info($photo_id, $format = 'php_serial') {
        $args = array(
            'method' => 'flickr.photos.getSizes',
            'api_key' => $this->apiKey,
            'photo_id' => $photo_id,
            'format' => $format,
        );

        $url = 'http://flickr.com/services/rest/?';
        $search = $url.http_build_query($args);
        $result = $this->file_get_contents_curl($search);
        if ($format == 'php_serial') $result = unserialize($result);

        //Isolate only the video related information
        $video_info = [];
        foreach ($result['sizes']['size'] as $item) {
            if ($item['media'] == 'video' && !strpos($item['source'], 'stewart.swf')) {
                array_push($video_info, $item);
            }
        }

        return $video_info;
    }

    private function get_album_list_data($user_id = null, $page = 1, $per_page = 500, $format = 'php_serial')
    {
        $args = array(
            'method' => 'flickr.photosets.getList',
            'api_key' => $this->apiKey,
            'user_id' => $user_id,
            'page' => $page,
            'per_page' => $per_page,
            'format' => $format,
        );

        $url = 'http://flickr.com/services/rest/?';
        $search = $url . http_build_query($args);
        $result = $this->file_get_contents_curl($search);
        if ($format == 'php_serial') $result = unserialize($result);

        return $result;
    }

    public function generate_album_cache($user_id = null, $page = 1, $per_page = 500, $format = 'php_serial')
    {
        //Build the array holding the album info indexed by album id
        $page_count = 20; //to be updated once the query is executed
        for ($page = 1; $page <= $page_count; $page++) {
            msg("Fetching album list page $page");
            $current_data = $this->get_album_list_data(FLICKR_USER_NAME, $page);
            $page_count = $current_data['photosets']['pages'];

            //Save the fetched albums to the album list
            foreach ($current_data['photosets']['photoset'] as $album) {
                $this->album_list[$album['id']] = $album;
                $this->album_list[$album['id']]['collection'] = ''; //add this for later
            }
        }

        //Build the array holding the album items indexed by photo id
        foreach ($this->album_list as $album) {
            $page_count = 20; //to be updated once the query is executed
            for ($page = 1; $page <= $page_count; $page++) {
                msg("Fetching album " . $album['id'] . " items page $page");
                $current_data = $this->get_album_items($album['id'], FLICKR_USER_NAME, $page);
                $page_count = $current_data['photoset']['pages'];

                //Save the fetched album items
                foreach ($current_data['photoset']['photo'] as $photo) {
                    if (array_key_exists($photo['id'], $this->album_items))
                        array_push($this->album_items[$photo['id']], $album['id']);
                    else
                        $this->album_items[$photo['id']] = array($album['id']);
                }
            }
        }

        //Build the array holding the collections indexed by collection id
        msg("Fetching collection list");
        $current_data = $this->get_collections(FLICKR_USER_NAME);
        foreach ($current_data['collections']['collection'] as $collection) {
            //Save the fetched collections to the collection list
            $this->collection_list[$collection['id']] = $collection;
            //Link the collection's albums to the collection
            foreach ($collection['set'] as $album) {
                $this->album_list[$album['id']]['collection'] = $collection['id'];
            }
        }

        //Save the fetched album & collection data in cached JSON files
        msg("Saving JSON files");
        file_put_contents("cached_album_list.json", json_encode($this->album_list));
        file_put_contents("cached_album_items.json", json_encode($this->album_items));
        file_put_contents("cached_collection_list.json", json_encode($this->collection_list));
    }

    public function read_album_cache()
    {
        //Read the cached album & collection data
        $this->album_list = json_decode(file_get_contents("cached_album_list.json"), true);
        $this->album_items = json_decode(file_get_contents("cached_album_items.json"), true);
        $this->collection_list = json_decode(file_get_contents("cached_collection_list.json"), true);
    }

    private function get_album_items($album_id = null, $user_id = null, $page = 1, $per_page = 500, $format = 'php_serial')
    {
        $args = array(
            'method' => 'flickr.photosets.getPhotos',
            'api_key' => $this->apiKey,
            'photoset_id' => $album_id,
            'user_id' => $user_id,
            'page' => $page,
            'per_page' => $per_page,
            'format' => $format,
        );

        $url = 'http://flickr.com/services/rest/?';
        $search = $url . http_build_query($args);
        $result = $this->file_get_contents_curl($search);
        if ($format == 'php_serial') $result = unserialize($result);

        return $result;
    }

    private function get_collections( $user_id = null, $format = 'php_serial' )
    {
        $args = array(
            'method' => 'flickr.collections.getTree',
            'api_key' => $this->apiKey,
            'user_id' => $user_id,
            'format' => $format,
        );

        $url = 'http://flickr.com/services/rest/?';
        $search = $url . http_build_query($args);
        $result = $this->file_get_contents_curl($search);
        if ($format == 'php_serial') $result = unserialize($result);

        return $result;
    }

    public function get_album_count($photo, &$album_count)
    {
        if ($this->album_items[$photo['id']] == null)
            return;
        //record the albums for this photo in the day's album count
        foreach ($this->album_items[$photo['id']] as $current_album) {
            if ($album_count[$current_album] == null) {
                $album_count[$current_album] = 1;
            } else {
                $album_count[$current_album]++;
            }
        }
    }

    public function get_album_summary($album_count, $day_count)
    {
        //Leave if there is nothing to summarize
        if ($day_count == 0) return;

        //Build html list summarizing the albums for the day
        $html = '<p style="margin-bottom: 0em;">Album Summary:' . "\n<ul>\n";
        $items_not_in_album = $day_count;
        foreach ($album_count as $album_id => $item_count) {
            $album_title = $this->album_list[$album_id]['title']['_content'];
            $album_total_items = $this->album_list[$album_id]['count_photos'] + $this->album_list[$album_id]['count_videos'];
            $owner = $this->album_list[$album_id]['owner'];
            $html .= "<li><a href=\"https://flickr.com/photos/$owner/albums/$album_id/\" target=\"_blank\">" .
                     "$album_title</a> - $item_count " . get_plural($item_count, "item") . " of $album_total_items total" . 
                     $this->get_collection_link_for_album($album_id) . "</li>\n";
            $items_not_in_album -= $album_total_items;
        }
        //Notify if any items are not in an album
        if ($items_not_in_album > 0) {
            $html .= "<li>$items_not_in_album " . get_plural($items_not_in_album, "item") . " not found in an album.</li>\n";
        }
        $html .= '</ul></p>';

        return $html;
    }

    public function get_collection_link_for_album($album_id)
    {
        //Ignore & leave if the album is not in a collection
        if ($this->album_list[$album_id]['collection'] == '') return '';

        //Otherwise, return the link html for the collection
        $owner = $this->album_list[$album_id]['owner'];
        $collection_id = explode("-", $this->album_list[$album_id]['collection'])[1];
        $collection_title = $this->collection_list[$this->album_list[$album_id]['collection']]['title'];

        return " - in collection <a href=\"https://flickr.com/photos/$owner/collections/$collection_id/\" " . 
               "target=\"_blank\">$collection_title</a>";
    }

    public function get_html_output($photo) {
        //output specific to photos or videos
        switch ($photo['media']) {
            case 'photo': //link to picture in Flickr
                $html = '<a href="' . $this->photo_url($photo) . '" target="_blank">' .
                    //image URL for large photo size in Flickr                                                 
                    '<img src="' . 'https://farm' . $photo["farm"] . '.static.flickr.com/' . $photo["server"] . '/' .
                    $photo["id"] . '_' . $photo["secret"] . '_b.jpg"/></a>';  //b = large; t = thumbnails
                break;

            case 'video': //show video player
                $video_width = $photo["o_width"] < 1023 ? $photo["o_width"] : 1023;
                $html = "<video controls width=\"$video_width\">\n";
                foreach ($photo['video_info'] as $source) {
                    $html .= '    <source src="' . $source['source'] . "\"/>\n";
                }
                $html .= '</video>';
                break;
        }

        //photo title
        $html .= "<br/>\n<a href=\"" . $this->photo_url($photo) . '" target="_blank">' . $photo["title"] . '</a> | ' .
            //number of views            
            $photo["views"] . ' ' . get_plural($photo["views"], "view") . ' | ' .
            //link to google maps using the photo's location
            '<a href="https://www.google.com/maps/search/' . $photo["latitude"] . ',' . $photo["longitude"] . '">Map</a> | ' .
            //Album link
            "Album: " . $this->get_album_link_for_photo($photo['id']) . "<br/>\n" .
            //photo long description
            $photo["description"]["_content"] . '<br/><br/>';

        return $html;
    }

    public static function photo_url($photo) {
        return "https://www.flickr.com/photos/" . $photo["owner"] . "/" . $photo["id"];
    }

    public function get_album_link_for_photo($photo_id)
    {
        $html = '';

        //Add link for each album this photo is in
        foreach ($this->album_items[$photo_id] as $current_album_id) {
            $album_title = $this->album_list[$current_album_id]['title']['_content'];
            $owner = $this->album_list[$current_album_id]['owner'];
            if ($html != '') $html .= ', '; //separate multiple albums
            $html .= "<a href=\"https://flickr.com/photos/$owner/albums/$current_album_id/\" target=\"_blank\">$album_title</a>";
        }

        //Return either link(s) or none
        if ($html != '') return $html;
        else return '(none)';
    }
}

?>