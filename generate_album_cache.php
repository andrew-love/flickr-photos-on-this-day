<?php
//Include the page theme header
require_once('theme_header.php');

//Other includes
require_once('config.php');
require_once('flickr.php');
require_once('functions.php');

echo "\n<h1 class=\"entry-title\"><u>Generating Cached Album Data</u></h1>\n";

msg("Accessing the Flickr API");
$Flickr = new Flickr(FLICKR_API_KEY);  //Setup the API object

//Get the album list and items in each album and save those to JSON files to be used later
msg("Generating the album cache files");
$Flickr->generate_album_cache(FLICKR_USER_NAME);

msg("Processing completed");

//Include the page theme footer
require_once('theme_footer.php');

?>