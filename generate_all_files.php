<?php
//Include the page theme header
require_once('theme_header.php');

//Other includes
require_once('config.php');
require_once('flickr.php');
require_once('rsswriter.php');
require_once('htmlwriter.php');
require_once('functions.php');

echo "\n<h1 class=\"entry-title\"><u>Generating All Files</u></h1>\n";

//Prepare calendar for storing the retrieved photo data
msg("Preparing calendar");
$calendar = [];
for ($month = 1; $month <= 12; $month++) {
    $calendar[$month] = [];
    for ($day = 1; $day <= cal_days_in_month(CAL_GREGORIAN, $month, 2024); $day++) { //used an arbitrary leap year to get max days
        $calendar[$month][$day] = array("count" => 0, "photos" => [], "year_count" => [], "album_count" => []);
    }
}

msg("Accessing the Flickr API");
$Flickr = new Flickr(FLICKR_API_KEY);  //Setup the API object

//First, get the album list, items in each album, and collection list
msg("Reading album & collection data from cache");
$Flickr->read_album_cache();

//Fetch all the Flickr photo data and store in calendar
$total = 0;
for ($year = START_YEAR; $year <= END_YEAR; $year++) {    // Start in configured start year and get every year's photos until the configured end year
    $page_count = 20; //to be updated once the query is executed
    for ($page = 1; $page <= $page_count; $page++) {
        //Check the date for photos
        msg("Fetching year $year page $page");
        $current_data = $Flickr->search('', FLICKR_USER_NAME, 500, null, $year, $page);
        $page_count = $current_data['photos']['pages'];

        //Save the fetched photos to the calendar
        foreach ($current_data['photos']['photo'] as $photo) {
            $month = (int)substr($photo['datetaken'], 5, 2);
            $day = (int)substr($photo['datetaken'], 8, 2);

            $calendar[$month][$day]['count']++;
            if ($photo['media'] == 'video') { //get the video URL data
                msg('Fetching video info for ID ' . $photo['id']);
                $photo['video_info'] = $Flickr->get_video_info($photo['id']);
            }
            array_push($calendar[$month][$day]['photos'], $photo);
            if ($calendar[$month][$day]['year_count'][$year] == null) {
                $calendar[$month][$day]['year_count'][$year] = 1;
            }
            else {
                $calendar[$month][$day]['year_count'][$year]++;
            }
            $Flickr->get_album_count($photo, $calendar[$month][$day]['album_count']);
            $total++;
        }
    }
}

//For each day of the year generate a page with the photos taken that day
msg("Generating pages for each day of the year");
for ($month = 1; $month <= 12; $month++) {
    for ($day = 1; $day <= cal_days_in_month(CAL_GREGORIAN, $month, 2024); $day++) { //used an arbitrary leap year to get max days
        msg("Generating $month-$day.html");
        $Html = new HtmlWriter("$month-$day.html");
        $Html->open_file();
        $Html->navigation_links($month, $day, 'top');
        $Html->day_header($month, $day, $calendar[$month][$day]['count'], count($calendar[$month][$day]['year_count']));
        $Html->add_string($Flickr->get_album_summary($calendar[$month][$day]['album_count'], $calendar[$month][$day]['count']) . "\n");
        $year = '';
        if ($calendar[$month][$day]['count'] > 0) {
            //Generate all the photo page data
            foreach ($calendar[$month][$day]['photos'] as $photo) {
                if (substr($photo['datetaken'], 0, 4) != $year) {
                    //Header for each year with results
                    $year = substr($photo['datetaken'], 0, 4);
                    $Html->year_header(substr($photo['datetaken'], 0, 10), $calendar[$month][$day]['year_count'][$year]);
                }
                //Output the current photo
                $Html->add_string($Flickr->get_html_output($photo) . "\n");
            }
        } 
        else {
            //Handle HTML output if no photos were found
            $Html->add_string('No photos taken on ' . date("F", mktime(0, 0, 0, $month, 1)) . ' ' . get_ordinal($day) . "\n");
        }
        $Html->navigation_links($month, $day, 'bottom');
        $Html->close_file();
    }
}

//Generate the today file
msg("Generating today.html");
copy((int) date("m") . "-" . (int) date("d") . ".html", "today.html");

//Generate the RSS feed using the current day's data
msg("Generating " . RSS_FILE_NAME);
$Rss = new RssWriter(RSS_FILE_NAME);
$Rss->open_file("Flickr Photos On This Day", SITE_URL, "RSS Feed of Flickr photos for the given day of the year");
$year = (int)date("Y");
$month = (int)date("m");
$day = (int)date("d");
$summaryTitle = $Rss::summary_title($month, $day, $calendar[$month][$day]['count'], count($calendar[$month][$day]['year_count']));
if ($calendar[$month][$day]['count'] > 0) {
    //Add a summary item
    $Rss->add_item($summaryTitle, SITE_URL . "today.html#" . date("Y-m-d"),
                   $Rss::summary_body($summaryTitle, 
                                      $Flickr->get_album_summary($calendar[$month][$day]['album_count'], $calendar[$month][$day]['count'])));

    foreach ($calendar[$month][$day]['photos'] as $photo) {
        $years_ago = $year - (int) substr($photo['datetaken'], 0, 4);
        $Rss->add_item(get_years_ago($years_ago) . " - " . date("m/d") . "/" . (int) substr($photo['datetaken'], 0, 4) . " - " . $photo["title"],
                       $Flickr::photo_url($photo) . "#" . date("Y-m-d"),
                       $Flickr->get_html_output($photo));
    }
}
else { //handle if no photos exist
    $Rss->add_item($summaryTitle, SITE_URL . "#" . date("Y-m-d"),
                   "No photos taken on " . date("F", mktime(0, 0, 0, $month, 1)) . ' ' . get_ordinal($day));
}
$Rss->close_file();

//Generate the index page with links to every day
msg("Generating index.html");
$Html = new HtmlWriter("index.html");
$Html->open_file();
$Html->add_string("<h1 class=\"entry-title\"><u>Flickr Photos On Each Day Of The Year - $total " . get_plural($total, "Photo") . "</u></h1><br/>\n");
for ($month = 1; $month <= 12; $month++) {
    $Html->add_string("<h2><u>" . date("F", mktime(0, 0, 0, $month, 1)) . "</u></h2><br/>\n");
    for ($day = 1; $day <= cal_days_in_month(CAL_GREGORIAN, $month, 2024); $day++) { //used an arbitrary leap year to get max days
        $Html->day_link($month, $day, $calendar[$month][$day]['count'], count($calendar[$month][$day]['year_count']));
    }
    $Html->add_string("<br/>\n");
}
$Html->close_file();
msg("Processing completed");

//Include the page theme footer
require_once('theme_footer.php');

?>