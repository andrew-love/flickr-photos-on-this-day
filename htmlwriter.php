<?php

class HtmlWriter
{
    private $filename, $htmlfile;

    public function __construct($file_in = null)
    {
        //Remember the file name for writing later
        $this->filename = $file_in;
    }

    public function open_file()
    {
        //Create HTML file
        $this->htmlfile = fopen($this->filename, "w") or die("Unable to open html file!");

        //First, include the theme header
        fwrite($this->htmlfile, file_get_contents("theme_header.php"));
    }

    public function navigation_links($month, $day, $location)
    {
        //Get the previous and next date filenames
        $previous_file = $this->get_adjacent_filename($month, $day, 'PREV');
        $next_file = $this->get_adjacent_filename($month, $day, 'NEXT');

        //Write out the navigation links
        fwrite($this->htmlfile, "<table class=\"navlinks $location\">\n");
        fwrite($this->htmlfile, "    <tr class=\"navlinks\">\n");
        fwrite($this->htmlfile, "        <td class=\"navlinks previous\"><a href=\"$previous_file\">\u{00AB} Previous Day</a></td>\n"); //U+00AB = �
        fwrite($this->htmlfile, "        <td class=\"navlinks next\"><a href=\"$next_file\">Next Day \u{00BB}</a></td>\n");             //U+00BB = �
        fwrite($this->htmlfile, "    </tr>\n");
        fwrite($this->htmlfile, "</table>\n");
    }

    public function day_header($month, $day, $count, $numberOfYears)
    {
        $month_name = date("F", mktime(0, 0, 0, $month, 1));
        $day_ord = get_ordinal($day);

        //Write the header for the day of the year
        if ($count > 0) {
            $photos = get_plural($count, "Photo");
            $years = get_plural($numberOfYears, "Year");
            fwrite($this->htmlfile, "\n<h1 class=\"entry-title\"><u>Flickr Photos On $month_name $day_ord - $count $photos from $numberOfYears $years</u></h1>\n");
        } else {
            fwrite($this->htmlfile, "\n<h1 class=\"entry-title\"><u>Flickr Photos On $month_name $day_ord</u></h1>\n");
        }
    }

    public function year_header($date_in, $count)
    {
        $years_ago = date("Y") - (int) substr($date_in, 0, 4);
        $date = date_create($date_in);
        $formatted_date = date_format($date, "m/d/Y");
        $photos = get_plural($count, "Photo");
        //Write the sub header for the year
        fwrite($this->htmlfile, "<h2>" . get_years_ago($years_ago) . " - $formatted_date - $count $photos</h2>\n");
    }

    public function day_link($month, $day, $count, $numberOfYears)
    {
        $month_name = date("F", mktime(0, 0, 0, $month, 1));
        $day_ord = get_ordinal($day);

        //Write the link for the day of the year
        if ($count > 0) {
            $photos = get_plural($count, "Photo");
            $years = get_plural($numberOfYears, "Year");
            fwrite($this->htmlfile, "<a href=\"$month-$day.html\">$month_name $day_ord</a> - $count $photos from $numberOfYears $years<br/>\n");
        } else {
            fwrite($this->htmlfile, "<a href=\"$month-$day.html\">$month_name $day_ord</a><br/>\n");
        }
    }

    public function add_string($string_in)
    {
        //Write the string data
        fwrite($this->htmlfile, $string_in);
    }

    public function close_file()
    {
        //Include the theme footer
        fwrite($this->htmlfile, file_get_contents("theme_footer.php"));

        //Close the HTML file
        fclose($this->htmlfile);
    }

    private function get_adjacent_filename($month, $day, $direction)
    {
        //Get requested adjacent date file name
        $date = date_create("2024-$month-$day"); //used an arbitrary leap year

        if ($direction == 'PREV') {
            date_sub($date, date_interval_create_from_date_string('1 day'));
        }
        else if ($direction == 'NEXT') {
            date_add($date, date_interval_create_from_date_string('1 day'));
        }
        else {
            return '';
        }

        return (int) date_format($date, "m") . '-' . (int) date_format($date, "d") . '.html';
    }
}

?>